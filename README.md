# Eshop s módou
Vytvořte e-shop na prodej oblečení a módních doplňků. Uživatel bude moct objednat výrobky z nabízeného sortimentu. Po dokončení objednávky bude uživateli na zadaný e-mail odesláno potvrzení objednávky. Administrátor bude moct přidávat nové výrobky a odebírat prodané. Po přijetí objednávky mu bude doručen e-mail s objednávkou, veškeré objednávky budou také vypsané na samostatné stránce.  


Hlavní stránka eshopu : http://prmat.8u.cz/Prmat/templates/shopAboutus2.latte
Adminstrace: http://prmat.8u.cz/Prmat/administrace/loginscreen.latte
Přihlašovací údaje:
jméno: admin
heslo: admin
